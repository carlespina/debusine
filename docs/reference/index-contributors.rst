.. _reference-contributors:

===================================
Reference for debusine contributors
===================================

.. toctree::

   design-goals
   Team organization <development-team-organization>
   design-practices
   coding-practices
   release-note-practices
   documentation-practices
   internal-api/index

