.. _available-bare-data:

Available bare data items
=========================

.. toctree::
   :glob:

   specs/*
