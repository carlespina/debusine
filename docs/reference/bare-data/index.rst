.. _bare-data-reference:

=========
Bare data
=========

Some :ref:`collections <explanation-collections>` support "bare data items",
which only store metadata and do not link to artifacts or to other
collections.  These items are organized by category in the same way as
artifacts and collections.

.. toctree::

    specs
