.. _development-blueprints:

======================
Development blueprints
======================

.. toctree::

    permissions
    permission-predicates
    ui-views
    package-upload
    task-configuration
    task-statistics
    build-instructions
    scoped-urls
    debian-pipeline
    url-redesign
    url-user-redesign
    dynamic-compute
    experiment-workspaces
    roles-permissions-on-groups
