.. _asset-cloud-provider-account:

Category: ``debusine:cloud-provider-account``
=============================================

This asset stores details of a cloud provider account to be used by this
debusine instance.

The details of the data in this asset are subject to change until at least
two providers have been implemented.

* Data:

  * ``provider_type`` (string): an item from an enumeration of supported
    providers
  * ``name`` (string): the name of the provider account
  * ``configuration`` (dictionary): non-secret provider-dependent
    information needed to manage instances (e.g. region name, entry point
    URLs)
  * ``credentials`` (dictionary): secret provider-dependent credentials
    needed to manage instances

For ``provider_type: aws``:

  * ``configuration``:

    * ``region_name``: name of AWS region (e.g. ``eu-west-1``)
    * ``s3_endpoint_url``: optional S3 endpoint URL (e.g.
      ``https://s3.eu-west-2.amazonaws.com/``); this can be used to work
      around `bucket propagation delays
      <https://repost.aws/knowledge-center/s3-http-307-response>`__

  * ``credentials`` (see `Manage access keys for IAM users
    <https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html>`__
    in the AWS documentation):

    * ``access_key_id``: 20-character string
    * ``secret_access_key``: 40-character string

Only a single asset can exist for a given account name.

At present, only instance administrators and the relevant debusine backend
code can create, modify, or access this category of asset: ``can_display``
should always return False, so that it can only be displayed in contexts
that disable permission checks.  In future, this may be opened up to scope
administrators for non-instance-wide provider accounts.
