.. _assets:

======
Assets
======

Assets key-value data is structured according to the category of the
asset.

Assets have roles directly associated with them. Each asset also has
usage roles associated with a workspace.

Data key names are used in ``pydantic`` models, and must therefore be
valid Python identifiers.

See below for technical details.

.. toctree::

    specs
