.. _runtime-environment:

===================
Runtime environment
===================

debusine is primarily developed to run on Debian 12 'Bookworm'
with all the dependencies coming from the standard Debian
repositories.  Use the bookworm-backports repository to be able to use the
required Django version (4.2).

To run debusine please use the provided packages.
