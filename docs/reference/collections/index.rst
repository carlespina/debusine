.. _collection-reference:

===========
Collections
===========

Collections are abstract aggregates of artifacts/collections/data. To be
able to make meaningful use of the system, they need to be assigned
categories documenting their intended use case.

The :ref:`available-collections` section documents the existing categories
and what you can expect from collections of each category. The allowed
collection items, the structure of the metadata, and the supported lookups
all depend on the category of the collection.

See the :ref:`explanation <explanation-collections>` for an overview, and
below for technical details.

.. toctree::

    data-models
    lookup
    singleton
    derived
    specs
