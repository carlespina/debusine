.. _file-store-reference:

===========
File stores
===========

See the :ref:`explanation <explanation-file-stores>` for an overview of file
stores, and below for technical details.

.. toctree::

    policies
    specs
