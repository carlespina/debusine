.. _file-backend-s3:

S3 file backend
===============

To use this backend, the store must have its ``provider_account`` set to
point to :ref:`an asset storing details of an AWS account
<asset-cloud-provider-account>`.

* Configuration:

  * ``bucket_name``: (string): The name of the S3 bucket to use.
  * ``storage_class`` (string, optional): The `storage class
    <https://docs.aws.amazon.com/AmazonS3/latest/userguide/storage-class-intro.html>`__
    to use for newly-uploaded objects.

* Supports returning local paths: no
* Supports returning URLs: yes (returns short-lived presigned URLs)
