===============
Building blocks
===============

If you are working with debusine, chances are that you are looking to
automate something with a custom workflow. To be able to effectively
leverage debusine, discover all the building blocks that you can combine
together.

This section lists all the artifacts that debusine can manipulate, all the
collections that can be used to store them, all the standard workflows
that you can combine and all the tasks that you will encounter inside
workflows (and that you can use to build new workflows).

.. toctree::

   artifacts/index
   assets/index
   bare-data/index
   collections/index
   file-stores/index
   tasks/index
   workflows/index
