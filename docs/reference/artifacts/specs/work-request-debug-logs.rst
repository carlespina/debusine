.. _artifact-work-request-debug-logs:

Category ``debusine:work-request-debug-logs``
=============================================

* Data: empty

* Files:

  * any number of files containing logs and information to help a debusine user
    understand the WorkRequest output: commands executed, output of the
    commands, etc.

* Relationships:

  * relates-to: the corresponding ``debian:source-package`` (if built from a
    source package)
