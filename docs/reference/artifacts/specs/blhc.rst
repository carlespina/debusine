.. _artifact-blhc:

Category ``debian:blhc``
========================

* Data: empty

* Files:

  * One blhc output file called ``blhc.txt`` for the corresponding input build log.

* Relationships:

  * relates-to: the corresponding ``debian:package-build-log`` input artifact.
