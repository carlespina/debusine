=============
Miscellaneous
=============

This section collects reference documentation that has no better place in
the rest of the hierarchy.

.. toctree::

   lookup-syntax
   faq
