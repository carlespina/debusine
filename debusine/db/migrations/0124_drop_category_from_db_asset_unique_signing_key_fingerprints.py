# Generated by Django 5.1.5 on 2025-02-05 18:45

from django.db import migrations, models

import debusine.assets.models
import debusine.db.constraints


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0123_asset_cloud_provider_account'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='asset',
            name='db_asset_unique_signing_key_fingerprints',
        ),
        migrations.AddConstraint(
            model_name='asset',
            constraint=debusine.db.constraints.JsonDataUniqueConstraint(
                condition=models.Q(
                    (
                        'category',
                        debusine.assets.models.AssetCategory['SIGNING_KEY'],
                    )
                ),
                fields=("data->>'fingerprint'",),
                name='db_asset_unique_signing_key_fingerprints',
                nulls_distinct=False,
            ),
        ),
    ]
