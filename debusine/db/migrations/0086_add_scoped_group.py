# Generated by Django 3.2.19 on 2024-10-03 13:58

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('db', '0085_scope_name_validation'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                ('name', models.CharField(max_length=255)),
                (
                    'scope',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to='db.scope',
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        blank=True,
                        related_name='debusine_groups',
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.AddConstraint(
            model_name='group',
            constraint=models.UniqueConstraint(
                fields=('name', 'scope'), name='db_group_unique_name_scope'
            ),
        ),
    ]
