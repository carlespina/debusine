# Copyright © The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for management/utils.py."""

import datetime

from django.test import TestCase

from debusine.server.management import utils


class UtilsTests(TestCase):
    """Tests for methods in management/utils.py."""

    def test_column_to_data(self) -> None:
        """Test Column.to_data."""
        col = utils.Column("foo", "Foo")
        for val in (None, 1, "str", datetime.datetime.today()):
            with self.subTest(val=val):
                self.assertIs(col.to_data(val), val)

    def test_column_to_rich(self) -> None:
        """Test Column.to_rich."""
        col = utils.Column("foo", "Foo")
        dt = datetime.datetime.today()
        for val, expected in (
            (None, "-"),
            (1, "1"),
            ("str", "str"),
            (dt, dt.isoformat()),
        ):
            with self.subTest(val=val):
                self.assertEqual(col.to_rich(val), expected)

    def test_attrcolumn_to_data(self) -> None:
        """Test Column.to_data."""
        self.value = 42
        col = utils.AttrColumn("value", "Value", "value")
        self.assertIs(col.to_data(self), 42)
