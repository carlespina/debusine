# Copyright © The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the management command worker_pool."""

from io import StringIO
from unittest import mock

import yaml
from django.core.management.base import CommandError
from django.test import override_settings

from debusine.assets import DummyProviderAccountData
from debusine.db.models import Worker, WorkerPool
from debusine.django.management.tests import call_command
from debusine.server.management.commands.worker_pool import Command
from debusine.server.worker_pools import (
    DummyWorkerPool,
    DummyWorkerPoolSpecification,
    WorkerPoolLimits,
)
from debusine.test.django import TestCase


@override_settings(LANGUAGE_CODE="en-us")
class WorkerPoolCommandTests(TestCase):
    """Tests for the `worker_pool` command."""

    def test_create_defaults_from_stdin(self) -> None:
        name = "test"
        provider_account = self.playground.create_cloud_provider_account_asset()
        provider_account_data = provider_account.data_model
        assert isinstance(provider_account_data, DummyProviderAccountData)
        specifications = DummyWorkerPoolSpecification(features=["foo"])

        stdout, stderr, exit_code = call_command(
            "worker_pool",
            "create",
            name,
            "--provider-account",
            provider_account_data.name,
            "--architectures",
            "amd64,i386",
            "--specifications",
            "-",
            stdin=StringIO(yaml.safe_dump(specifications.dict())),
        )

        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

        pool = WorkerPool.objects.get(name=name)
        self.assertTrue(pool.enabled)
        self.assertEqual(pool.architectures, ["amd64", "i386"])
        self.assertEqual(pool.tags, [])
        self.assertEqual(pool.provider_account, provider_account)
        self.assertEqual(pool.specifications, specifications.dict())
        self.assertTrue(pool.instance_wide)
        self.assertFalse(pool.ephemeral)
        self.assertEqual(pool.limits, WorkerPoolLimits().dict())

    def test_create_all_args_from_files(self) -> None:
        name = "test"
        provider_account = self.playground.create_cloud_provider_account_asset()
        provider_account_data = provider_account.data_model
        assert isinstance(provider_account_data, DummyProviderAccountData)
        specifications = DummyWorkerPoolSpecification(features=["something"])
        specifications_file = self.create_temporary_file(
            contents=specifications.json().encode()
        )

        limits = WorkerPoolLimits(max_idle_seconds=60)
        limits_file = self.create_temporary_file(
            contents=limits.json().encode()
        )

        stdout, stderr, exit_code = call_command(
            "worker_pool",
            "create",
            name,
            "--provider-account",
            provider_account_data.name,
            "--no-enabled",
            "--architectures",
            "i386",
            "--tags",
            "tag-1",
            "--specifications",
            str(specifications_file),
            "--no-instance-wide",
            "--ephemeral",
            "--limits",
            str(limits_file),
        )

        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

        pool = WorkerPool.objects.get(name=name)
        self.assertFalse(pool.enabled)
        self.assertEqual(pool.architectures, ["i386"])
        self.assertEqual(pool.tags, ["tag-1"])
        self.assertEqual(pool.provider_account, provider_account)
        self.assertEqual(pool.specifications, specifications.dict())
        self.assertFalse(pool.instance_wide)
        self.assertTrue(pool.ephemeral)
        self.assertEqual(pool.limits, limits.dict())

    def test_create_idempotent(self) -> None:
        pool = self.playground.create_worker_pool()
        specifications_file = self.create_temporary_file(
            contents=yaml.safe_dump(pool.specifications).encode()
        )
        limits = WorkerPoolLimits(max_idle_seconds=60)
        limits_file = self.create_temporary_file(
            contents=limits.json().encode()
        )
        stdout, stderr, exit_code = call_command(
            "worker_pool",
            "create",
            pool.name,
            "--provider-account",
            pool.provider_account.data["name"],
            "--no-enabled",
            "--architectures",
            "i386",
            "--tags",
            "tag-1",
            "--specifications",
            str(specifications_file),
            "--no-instance-wide",
            "--ephemeral",
            "--limits",
            str(limits_file),
        )

        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

        pool.refresh_from_db()

        self.assertFalse(pool.enabled)
        self.assertEqual(pool.architectures, ["i386"])
        self.assertEqual(pool.tags, ["tag-1"])
        self.assertFalse(pool.instance_wide)
        self.assertTrue(pool.ephemeral)
        self.assertEqual(pool.limits, limits.dict())

    def test_create_unknown_provider_account(self) -> None:
        with self.assertRaisesRegex(
            CommandError, r"Cloud provider account asset 'unknown' not found"
        ):
            call_command(
                "worker_pool",
                "create",
                "test",
                "--provider-account",
                "unknown",
            )

    def test_delete(self) -> None:
        pool = self.playground.create_worker_pool()
        stdout, stderr, exit_code = call_command(
            "worker_pool",
            "delete",
            pool.name,
        )

        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

        self.assertFalse(WorkerPool.objects.filter(name="test").exists())

    def test_delete_not_found(self) -> None:
        with self.assertRaisesRegex(
            CommandError, r"Worker Pool 'unknown' not found"
        ):
            call_command(
                "worker_pool",
                "delete",
                "unknown",
            )

    def test_delete_running_workers(self) -> None:
        pool = self.playground.create_worker_pool()
        self.playground.create_worker(worker_pool=pool)
        with self.assertRaisesRegex(
            CommandError, r"Worker pool 'test' has 1 running workers\."
        ):
            call_command(
                "worker_pool",
                "delete",
                "test",
            )

    def test_delete_running_workers_force(self) -> None:
        pool = self.playground.create_worker_pool()
        worker = self.playground.create_worker(worker_pool=pool)
        with mock.patch.object(
            DummyWorkerPool, "terminate_worker"
        ) as mock_terminate_worker:
            stdout, stderr, exit_code = call_command(
                "worker_pool",
                "delete",
                "test",
                "--force",
            )
        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)
        mock_terminate_worker.assert_called_once_with(worker)
        self.assertFalse(WorkerPool.objects.filter(name="test").exists())
        self.assertFalse(Worker.objects.filter(id=worker.id).exists())

    def test_enable(self) -> None:
        pool = self.playground.create_worker_pool(enabled=False)
        stdout, stderr, exit_code = call_command(
            "worker_pool",
            "enable",
            pool.name,
        )

        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

        pool.refresh_from_db()
        self.assertTrue(pool.enabled)

    def test_disable(self) -> None:
        pool = self.playground.create_worker_pool()
        stdout, stderr, exit_code = call_command(
            "worker_pool",
            "disable",
            pool.name,
        )

        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

        pool.refresh_from_db()
        self.assertFalse(pool.enabled)

    def test_unexpected_action(self) -> None:
        command = Command()

        with self.assertRaisesRegex(
            CommandError, r"Action 'does_not_exist' not found"
        ) as exc:
            command.handle(action="does_not_exist")

        self.assertEqual(getattr(exc.exception, "returncode"), 3)
