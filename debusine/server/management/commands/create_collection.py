# Copyright © The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine-admin command to create collections."""

import argparse
import sys
from typing import Any, NoReturn

from django.core.exceptions import ValidationError
from django.core.management import CommandError, CommandParser

from debusine.db.models import Collection, DEFAULT_WORKSPACE_NAME, Workspace
from debusine.django.management.debusine_base_command import DebusineBaseCommand


class Command(DebusineBaseCommand):
    """Command to create a collection."""

    help = "Create a new collection"

    def add_arguments(self, parser: CommandParser) -> None:
        """Add CLI arguments for the create_collection command."""
        parser.add_argument("name", help="Name")
        parser.add_argument("category", help="Category")
        parser.add_argument(
            "--workspace", help="Workspace", default=DEFAULT_WORKSPACE_NAME
        )
        parser.add_argument(
            "--data",
            type=argparse.FileType("r"),
            help=(
                "File path (or - for stdin) to read the data for the "
                "collection. YAML format. Defaults to stdin."
            ),
            default="-",
        )

    def cleanup_arguments(self, *args: Any, **options: Any) -> None:
        """Clean up objects created by parsing arguments."""
        if options["data"] != sys.stdin:
            options["data"].close()

    def handle(self, *args: Any, **options: Any) -> NoReturn:
        """Create the collection."""
        name = options["name"]
        category = options["category"]
        workspace_name = options["workspace"]
        data = self.parse_yaml_data(options["data"].read()) or {}

        try:
            workspace = Workspace.objects.get(name=workspace_name)
        except Workspace.DoesNotExist:
            raise CommandError(
                f'Workspace "{workspace_name}" not found', returncode=3
            )

        try:
            collection = Collection(
                name=name, category=category, workspace=workspace, data=data
            )
            collection.full_clean()
            collection.save()
        except ValidationError as exc:
            raise CommandError(
                "Error creating collection: " + "\n".join(exc.messages),
                returncode=3,
            )

        raise SystemExit(0)
