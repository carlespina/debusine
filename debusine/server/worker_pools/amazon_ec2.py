# Copyright © The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""API for provisioning and managing AWS EC2 cloud workers."""

from typing import TYPE_CHECKING

from debusine.assets import CloudProvidersType
from debusine.server.worker_pools.base import WorkerPoolInterface

if TYPE_CHECKING:
    from debusine.db.models import Worker


class AWSEC2WorkerPool(
    WorkerPoolInterface, cloud_provider=CloudProvidersType.AWS
):
    """Amazon EC2 Worker Pool."""

    def launch_worker(self, worker: "Worker") -> None:
        """Launch a cloud worker using worker."""
        raise NotImplementedError("There is no implementation, yet!")

    def terminate_worker(self, worker: "Worker") -> None:
        """Terminate the cloud worker specified."""
        raise NotImplementedError("There is no implementation, yet!")
