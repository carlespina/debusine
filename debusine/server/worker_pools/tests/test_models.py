# Copyright © The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Test the Worker Pool models."""

from unittest import TestCase

from debusine.server.worker_pools import (
    DummyWorkerPoolSpecification,
    worker_pool_specifications_model,
)


class TestWorkerPoolSpecifications(TestCase):
    """Tests for WorkerPoolSpecifications."""

    def test_worker_pool_specifications_model(self) -> None:
        model = DummyWorkerPoolSpecification(features=["something"])
        result = worker_pool_specifications_model(model.dict())
        self.assertEqual(model, result)
