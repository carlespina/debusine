# Copyright © The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Models used for worker pool data."""

import abc
from enum import StrEnum
from typing import Any, Literal

from debusine.assets import CloudProvidersType

try:
    import pydantic.v1 as pydantic
except ImportError:
    import pydantic as pydantic  # type: ignore


class BaseWorkerPoolDataModel(pydantic.BaseModel, abc.ABC):
    """Base pydantic model for worker pool configuration."""

    class Config:
        """Set up stricter pydantic Config."""

        validate_assignment = True
        extra = pydantic.Extra.forbid


_worker_pool_specifications_models: dict[
    CloudProvidersType, type["WorkerPoolSpecifications"]
] = {}


class WorkerPoolSpecifications(BaseWorkerPoolDataModel, abc.ABC):
    """Base class for WorkerPool.specifications data models."""

    provider_type: CloudProvidersType

    def __init_subclass__(
        cls,
        provider_type: CloudProvidersType,
        **kwargs: Any,
    ) -> None:
        """Register subclass in _worker_pool_specifications_models."""
        super().__init_subclass__(**kwargs)
        _worker_pool_specifications_models[provider_type] = cls


def worker_pool_specifications_model(
    data: dict[str, Any]
) -> WorkerPoolSpecifications:
    """Load the WorkerPoolSpecifications subclass model for data."""
    model = _worker_pool_specifications_models[data["provider_type"]]
    return model.parse_obj(data)


class WorkerPoolLimits(BaseWorkerPoolDataModel):
    """Specifications for limits on a WorkerPool."""

    max_active_instances: int | None = None
    target_max_seconds_per_month: int | None = None
    max_idle_seconds: int = 3600


class ScopeWorkerPoolLimits(BaseWorkerPoolDataModel):
    """Specifications for limits on a WorkerPool."""

    target_max_seconds_per_month: int | None = None
    target_latency_seconds: int | None = None


# Test Dummy:


class DummyWorkerPoolSpecification(
    WorkerPoolSpecifications, provider_type=CloudProvidersType.DUMMY
):
    """Specifications for a DummyWorkerPool."""

    provider_type: Literal[CloudProvidersType.DUMMY] = CloudProvidersType.DUMMY
    features: list[str] = []


# Amazon AWS EC2:


class AWSEC2InstanceType(StrEnum):
    """Type of AWS EC2 Instances to launch."""

    ON_DEMAND = "on-demand"
    SPOT = "spot"


class AWSEC2AllocationStrategy(StrEnum):
    """
    Allocation Strategy for AWS EC2 Instances.

    https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-fleet-allocation-strategy.html
    """

    # Both:
    LOWEST_PRICE = "lowest-price"
    # Spot:
    DIVERSIFIED = "diversified"
    CAPACITY_OPTIMIZED = "capacity-optimized"
    CAPACITY_OPTIMIZED_PRIORITIZED = "capacity-optimized-prioritized"
    PRICE_CAPACITY_OPTIMIZED = "price-capacity-optimized"
    # On-Demand:
    PRIORITIZED = "prioritized"


class AWSEC2LaunchTemplate(BaseWorkerPoolDataModel):
    """AWS EC2 Launch Template."""

    launch_template_id: str
    priority: int | None = None


class AWSEC2WorkerPoolSpecification(
    WorkerPoolSpecifications, provider_type=CloudProvidersType.AWS
):
    """Specifications for an AWS EC2 WorkerPool."""

    provider_type: Literal[CloudProvidersType.AWS] = CloudProvidersType.AWS
    launch_templates: list[AWSEC2LaunchTemplate]
    instance_type: AWSEC2InstanceType = AWSEC2InstanceType.SPOT
    allocation_strategy: AWSEC2AllocationStrategy = (
        AWSEC2AllocationStrategy.LOWEST_PRICE
    )
    # Spot:
    max_total_price: int | None = None
