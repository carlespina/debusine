#!/bin/sh

set -e

write_readconfig_script ()
{
    # this Python script is embedded here because the .config
    # will be run before the new version is unpacked, so we
    # can't ship the script in the .deb

    READ_CONFIG_SCRIPT=$(mktemp -t debusine-readconfig-XXXXXXXX.py) || exit 1
    chmod 755 "$READ_CONFIG_SCRIPT"
    cat > "$READ_CONFIG_SCRIPT" <<'EOF'
#!/usr/bin/env python3

import os
import shlex
import sys

sys.path.append("/etc/debusine/server/")

# work around maintainer script is run as root
FAKE_USER = "fake-debusine-random-value"
os.environ["LOGNAME"] = FAKE_USER

from db_postgresql import DATABASES

db = DATABASES["default"]

dbuser = db['USER'] if db['USER'] != FAKE_USER else "debusine-server"

print(f"dbc_dbname={shlex.quote(db['NAME'])}")
print(f"dbc_dbuser={shlex.quote(dbuser)}")
print(f"dbc_dbpass={shlex.quote(db['PASSWORD'])}")
print(f"dbc_dbserver={shlex.quote(db['HOST'])}")
print(f"dbc_dbport={shlex.quote(db['PORT'])}")
EOF
# end of the embedded Python script
} # write_readconfig_script()

# start of the actual command flow

# shellcheck source=/dev/null
. /usr/share/debconf/confmodule

# shellcheck disable=SC2034
if [ -f /usr/share/dbconfig-common/dpkg/config.pgsql ]; then
# shellcheck source=/dev/null
	. /usr/share/dbconfig-common/dpkg/config.pgsql

	dbc_first_version="0.4.2"
	dbc_dbuser="debusine-server"
	dbc_dbname="debusine"

	if [ -f "/etc/debusine/server/db_postgresql.py" ]; then
	    write_readconfig_script
	    dbc_load_include="exec:$READ_CONFIG_SCRIPT"
	fi

	dbc_go debusine-server "$@"
	rm -f "$READ_CONFIG_SCRIPT"
fi

db_beginblock
db_input medium debusine-server/configure-webserver || true
db_input medium debusine-server/restart-webserver || true
db_endblock
db_go

#DEBHELPER#
